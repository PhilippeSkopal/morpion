#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>   // pour std::string

using namespace std;
void DisplayTableTicTacToe(char * Table, int SizeTable);
void InitialetableTicTacToe(char * Table, int SizeTable);
int TranslatePlayerChoice(string strPlayerChoice);
bool CheckNonEmpty(char chrTableChar);

int main()
{
    int intUserCounter = 1;

    char TableTicTacToe[9];
    InitialetableTicTacToe(TableTicTacToe, 8);

    bool PlayGame = true;
    bool ValidPlayerChoice;
    int intPlayerChoice;
    while(PlayGame)
    {

        // Affiche le joueur qui doit jouer
        if((intUserCounter%2) == 1){
            //Joueur 1
            cout << "Tour du joueur 1" << endl;

        }else if((intUserCounter%2) == 0){
            //Joueur 2
            cout << "Tour du joueur 2" << endl;
        }

        // affiche,
        DisplayTableTicTacToe(TableTicTacToe, 8);

        // R�cup�re une entr�e, et boucler tant que elle n'estpas valide
        ValidPlayerChoice = false;
        intPlayerChoice = 0;
        do{
            cout << "Entrer le nom de la case choisis, une lettre suivis d'un chiffre : ";

            string strPlayerChoice;
            cin >>  strPlayerChoice;

            intPlayerChoice = TranslatePlayerChoice(strPlayerChoice);
            if(intPlayerChoice != -1 && CheckNonEmpty(TableTicTacToe[intPlayerChoice])){
                ValidPlayerChoice = true;
            }else{
                cout << "Entrer un nom de case valide ou vide." << endl;
            }
        }while(!ValidPlayerChoice);

        // Ajoute dans le tableau de choix, en fonction de l'user
        // Un 'O' si joueur deux
        // Un 'X' si joueur un

        if((intUserCounter%2) == 1 && ValidPlayerChoice){
            //Joueur 1
            TableTicTacToe[intPlayerChoice] = 'X';

        }else if((intUserCounter%2) == 0 && ValidPlayerChoice){
            //Joueur 2
            TableTicTacToe[intPlayerChoice] = 'O';
        }

        // teste si victoire
        ////cout << intPlayerChoice << endl;

        // La joueur � jouer, incr�menter pour possiblement changer de joueur
        intUserCounter++;

        //Pour ne pas boucler comme un con
        //PlayGame = false;
    }



    return 0;
}

void DisplayTableTicTacToe(char * Table, int SizeTable)
{
    cout << "    1   2   3" << endl;

    cout << " A " << "|" << Table[0]<< "| |" << Table[1]<< "| |" << Table[2]<< "|" << endl;
    cout << "   --- --- ---" << endl;
    cout << " B " << "|" << Table[3]<< "| |" << Table[4]<< "| |" << Table[5]<< "|" << endl;
    cout << "   --- --- ---" << endl;
    cout << " C " << "|" << Table[6]<< "| |" << Table[7]<< "| |" << Table[8]<< "|" << endl;
}

void InitialetableTicTacToe(char * Table, int SizeTable)
{
    int i;
    for(i = 0; i <= SizeTable ; i++ ){
        Table[i] =  ' ';
    }
}

int TranslatePlayerChoice(string strPlayerChoice)
{
    int intPlayerChoice;

    if(strPlayerChoice == "A1") intPlayerChoice =  0;
    if(strPlayerChoice == "A2") intPlayerChoice =  1;
    if(strPlayerChoice == "A3") intPlayerChoice =  2;
    if(strPlayerChoice == "B1") intPlayerChoice =  3;
    if(strPlayerChoice == "B2") intPlayerChoice =  4;
    if(strPlayerChoice == "B3") intPlayerChoice =  5;
    if(strPlayerChoice == "C1") intPlayerChoice =  6;
    if(strPlayerChoice == "C2") intPlayerChoice =  7;
    if(strPlayerChoice == "C3") intPlayerChoice =  8;

    if(intPlayerChoice >= 0 && intPlayerChoice <= 8){
        return intPlayerChoice;
    }else{
        return -1;
    }

}

bool CheckNonEmpty(char chrTableChar)
{
    if(chrTableChar == 'X' || chrTableChar == 'O' ){
        return false;
    }else{
        return true;
    }
}
